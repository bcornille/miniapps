// MFEM Kinetic Dynamo MiniApp
//
// Description:  This code solves the kinematic dynamo eigenvalue problem,
//               lambda*B = curl(v x B), with several possible variational
//               formulations.

#include "mfem.hpp"
#include <iostream>
#include <cmath>

// constant pi
const double pi = std::acos(-1);

// Choice for the problem setup. The fluid velocity is chosen by this parameter.
int problem;

// Velocity coefficient
void velocity_function(const mfem::Vector &x, mfem::Vector &v);

// Mesh bounding box
mfem::Vector bb_min, bb_max;

int main(int argc, char *argv[])
{
    // 1. Initialize MPI.
    int num_procs, myid;

	// Default command-line options.
	problem = 0;
	const char *mesh_file = "periodic-square.mesh";
    int ser_ref_levels = 2;
    int order = 1;
    int nev = 5;
    bool visualization = 1;
    // 2. Parse command-line options.
    mfem::OptionsParser args(argc, argv);
    args.AddOption(&mesh_file, "-m", "--mesh",
        "Mesh file to use.");
    args.AddOption(&problem, "-p", "--problem",
        "Problem setup to use. See options in velocity_function().");
    args.AddOption(&ser_ref_levels, "-rs", "--refine-serial",
        "Number of times to refine the mesh uniformly in serial.");
    args.AddOption(&order, "-o", "--order",
        "Order (degree) of the finite elements.");
    args.AddOption(&nev, "-n", "--num-eigs",
        "Number of desired eigenmodes.");
    args.AddOption(&visualization, "-vis", "--visualization", "-no-vis",
        "--no-visualization",
        "Enable or disable GLVis visualization.");
    args.Parse();
    if (!args.Good())
    {
        if (myid == 0)
        {
            args.PrintUsage(std::cout);
        }
        return 1;
    }
    if (myid == 0)
    {
        args.PrintOptions(std::cout);
    }

    // 3. Read the mesh from the given mesh file. We can handle geometrically
    //    periodic meshes in this code.
    mfem::Mesh *mesh = new mfem::Mesh(mesh_file, 1, 1);
    int dim = mesh->Dimension();
    if (dim != 3) {
        return 2;
    }

    // 4. Refine the mesh to increase the resolution. In this example we do
    //    'ref_levels' of uniform refinement, where 'ref_levels' is a
    //    command-line parameter. If the mesh is of NURBS type, we convert it to
    //    a (piecewise-polynomial) high-order mesh.
    for (int lev = 0; lev < ser_ref_levels; lev++)
    {
        mesh->UniformRefinement();
    }
    mesh->GetBoundingBox(bb_min, bb_max, std::max(order, 1));

    // 6. Define a parallel finite element space on the parallel mesh.
    mfem::FiniteElementCollection *h1_fec = new mfem::H1_FECollection(order, dim);
    mfem::FiniteElementSpace *h1_fes = new mfem::FiniteElementSpace(mesh, h1_fec, 3);

    mfem::VectorFunctionCoefficient velocity(3, velocity_function);

    if (visualization)
    {
        char vishost[] = "localhost";
        int  visport   = 19916;
        mfem::socketstream mode_sock(vishost, visport);
        mode_sock.precision(8);

        mfem::GridFunction *v_0 = new mfem::GridFunction(h1_fes);
        v_0->ProjectCoefficient(velocity);

        mode_sock << "solution\n" << *mesh << *v_0 << std::flush
            << "window_title 'Flow Field'" << std::endl;

        char c;
        if (myid == 0)
        {
            std::cout << "press (q)uit or (c)ontinue --> " << std::flush;
            std::cin >> c;
        }
        mode_sock.close();

        delete v_0;
    }

    delete h1_fes;
    delete h1_fec;
    delete mesh;

    return 0;
}

void velocity_function(const mfem::Vector &x, mfem::Vector &v)
{
    int dim = x.Size();

    // map to the reference [-1,1] domain
    mfem::Vector X(dim);
    for (int i = 0; i < dim; i++)
    {
        double center = (bb_min[i] + bb_max[i]) * 0.5;
        X(i) = 2 * (x(i) - center) / (bb_max[i] - bb_min[i]);
    }
    switch (problem)
    {
        case 0:
        {
            // Roberts flow
            v(0) = std::sqrt(2.)*std::sin(pi*X(0))*std::cos(pi*X(1));
            v(1) = -std::sqrt(2.)*std::cos(pi*X(0))*std::sin(pi*X(1));
            v(2) = 2.*std::sin(pi*X(0))*std::sin(pi*X(1));
            break;
        }
    }
}
