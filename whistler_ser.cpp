//                       Whistler Wave Eigenmodes
//
// Compile with: make whistler
//
// Sample runs:  whistler -m ../periodic-cube.mesh
//
// Description:  This example code solves the electron MHD
//               eigenvalue problem -curl(curl(b) x B_0) = lambda b with homogeneous
//               Dirichlet boundary conditions b * n = 0.

#include "mfem.hpp"
#include <fstream>
#include <iostream>
#include "densemat.hpp"

using namespace std;
using namespace mfem;

// Choice for the problem setup. The fluid velocity is chosen by this parameter.
int problem;

// Velocity coefficient
void b0field_function(const Vector &x, Vector &v);

// Mesh bounding box
Vector bb_min, bb_max;

int main(int argc, char *argv[])
{
   // 2. Parse command-line options.
   const char *mesh_file = "periodic-cube.mesh";
   problem = 1;
   int ser_ref_levels = 2;
   int order = 1;
   int nev = 5;
   bool visualization = 1;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&problem, "-p", "--problem",
                   "Problem setup to use. See options in velocity_function().");
   args.AddOption(&ser_ref_levels, "-rs", "--refine-serial",
                  "Number of times to refine the mesh uniformly in serial.");
   args.AddOption(&order, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   args.AddOption(&nev, "-n", "--num-eigs",
                  "Number of desired eigenmodes.");
   args.AddOption(&visualization, "-vis", "--visualization", "-no-vis",
                  "--no-visualization",
                  "Enable or disable GLVis visualization.");
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // 3. Read the (serial) mesh from the given mesh file on all processors. We
   //    can handle triangular, quadrilateral, tetrahedral, hexahedral, surface
   //    and volume meshes with the same code.
   Mesh *mesh = new Mesh(mesh_file, 1, 1);
   int dim = mesh->Dimension();
   if (dim != 3) {
        return 2;
    }

   // 4. Refine the serial mesh on all processors to increase the resolution. In
   //    this example we do 'ref_levels' of uniform refinement (2 by default, or
   //    specified on the command line with -rs).
   for (int lev = 0; lev < ser_ref_levels; lev++)
   {
      mesh->UniformRefinement();
   }
   mesh->GetBoundingBox(bb_min, bb_max, max(order, 1));

   // 6. Define a parallel finite element space on the parallel mesh. Here we
   //    use the Nedelec finite elements of the specified order.
   FiniteElementCollection *fec = new ND_FECollection(order, dim);
   FiniteElementSpace *fespace = new FiniteElementSpace(mesh, fec);
   int size = fespace->GetTrueVSize();
   cout << "Number of unknowns: " << size << endl;

   // 7. Set up the parallel bilinear forms a(.,.) and m(.,.) on the finite
   //    element space. The first corresponds to the curl curl, while the second
   //    is a simple mass matrix needed on the right hand side of the
   //    generalized eigenvalue problem below. The boundary conditions are
   //    implemented by marking all the boundary attributes from the mesh as
   //    essential. The corresponding degrees of freedom are eliminated with
   //    special values on the diagonal to shift the Dirichlet eigenvalues out
   //    of the computational range. After serial and parallel assembly we
   //    extract the corresponding parallel matrices A and M.
   VectorFunctionCoefficient b0field(dim, b0field_function);
   ConstantCoefficient one(1.0);
   // Array<int> ess_bdr;
   // if (mesh->bdr_attributes.Size())
   // {
   //    ess_bdr.SetSize(mesh->bdr_attributes.Max());
   //    ess_bdr = 1;
   // }

   BilinearForm *a = new BilinearForm(fespace);
   // a->AddDomainIntegrator(new MixedCrossCurlCurlIntegrator(b0field));
   a->AddDomainIntegrator(new CurlCurlIntegrator());
   // cout << mesh->bdr_attributes.Size() << endl;
   // if (mesh->bdr_attributes.Size() == 0)
   // {
   //    // Add a mass term if the mesh has no boundary, e.g. periodic mesh or
   //    // closed surface.
   //    a->AddDomainIntegrator(new VectorFEMassIntegrator(one));
   // }
   a->Assemble();
   // a->EliminateEssentialBCDiag(ess_bdr, 1.0);
   a->Finalize();

   BilinearForm *m = new BilinearForm(fespace);
   m->AddDomainIntegrator(new VectorFEMassIntegrator(one));
   m->Assemble();
   // shift the eigenvalue corresponding to eliminated dofs to a large value
   // m->EliminateEssentialBCDiag(ess_bdr, numeric_limits<double>::min());
   m->Finalize();

   DenseMatrix A = makeDenseFromSparse(a->SpMat());
   ofstream afile;
   afile.open("A_matrix.txt");
   A.PrintMatlab(afile);
   afile.close();
   DenseMatrix M = makeDenseFromSparse(m->SpMat());
   ofstream mfile;
   mfile.open("M_matrix.txt");
   M.PrintMatlab(mfile);
   mfile.close();

   GeneralEigensystem es(A, M);
   es.Eval();

   GridFunction x(fespace);

   // 11. Send the solution by socket to a GLVis server.
   if (visualization)
   {
      char vishost[] = "localhost";
      int  visport   = 19916;
      socketstream mode_sock(vishost, visport);
      mode_sock.precision(8);

      for (int i=0; i<nev; i++)
      {
         cout << "Eigenmode " << i+1 << '/' << nev
            << ", Lambda = " << es.eigenvalue(i) << endl;

         if (std::abs(es.eigenvalue(i)) < 14.0)
         {
            // convert eigenvector from degrees of freedom
            x = es.eigenvector_component(i);

            mode_sock << "solution\n" << *mesh << x << flush
                      << "window_title 'Eigenmode " << i+1 << '/' << nev
                      << ", Lambda = " << es.eigenvalue(i) << "'" << endl;

            char c;
            cout << "press (q)uit or (c)ontinue --> " << flush;
            cin >> c;

            if (c != 'c')
            {
              break;
            }
         }
      }
      // mode_sock.close();
   }

   // 12. Free the used memory.
   delete a;
   delete m;

   delete fespace;
   delete fec;
   delete mesh;

   return 0;
}

void b0field_function(const Vector &x, Vector &b)
{
   int dim = x.Size();

   // map to the reference [-1,1] domain
   Vector X(dim);
   for (int i = 0; i < dim; i++)
   {
      double center = (bb_min[i] + bb_max[i]) * 0.5;
      X(i) = 2 * (x(i) - center) / (bb_max[i] - bb_min[i]);
   }
   switch (problem)
   {
      case 0:
      {
         switch (dim)
         {
            case 3: b(2) = 1.0;
            case 2: b(1) = 1.0;
            case 1: b(0) = 1.0;
            break;
         }
         break;
      }
      default:
      {
         switch (dim)
         {
            case 3: b(2) = 0.0;
            case 2: b(1) = 0.0;
            case 1: b(0) = 1.0;
            break;
         }
         break;
      }
   }
}
