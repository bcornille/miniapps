#ifndef MYINTEGRATORS_HPP
#define MYINTEGRATORS_HPP

#include "mfem.hpp"

namespace mfem
{

class VectorCrossCurlCurlIntegrator : public BilinearFormIntegrator
{
private:
#ifndef MFEM_THREAD_SAFE
    Vector V;
    DenseMatrix dshape_hat, dshape, curlshape, crosscurlshape, Jadj;
#endif // MFEM_THREAD_SAFE
    VectorCoefficient *VQ;

public:
    VectorCrossCurlCurlIntegrator(VectorCoefficient &vq) : VQ(&vq) { }

    virtual void AssembleElementMatrix(const FiniteElement &el,
                                       ElementTransformation &Trans,
                                       DenseMatrix &elmat)
    {
        int dim = el.GetDim();
        int dof = el.GetDof();
        int cld = (dim*(dim-1))/2;

#ifdef MFEM_THREAD_SAFE
        Vector V(VQ ? VQ->GetVDim() : 0);
        DenseMatrix dshape_hat(dof, dim), dshape(dof, dim);
        DenseMatrix curlshape(dim*dof, cld), crosscurlshape(dim*dof, cld), Jadj(dim);
#else
        V.SetSize(VQ ? VQ->GetVDim() : 0);
        dshape_hat.SetSize(dof, dim);
        dshape.SetSize(dof, dim);
        crosscurlshape.SetSize(dim*dof, cld);
        curlshape.SetSize(dim*dof, cld);
        Jadj.SetSize(dim);
#endif

        const IntegrationRule *ir = IntRule;
        if (ir == NULL)
        {
            // use the same integration rule as diffusion
            int order = 2 * Trans.OrderGrad(&el);
            ir = &IntRules.Get(el.GetGeomType(), order);
        }

        elmat.SetSize(dof*dim);
        elmat = 0.0;
        for (int i = 0; i < ir->GetNPoints(); i++)
        {
            const IntegrationPoint &ip = ir->IntPoint(i);
            el.CalcDShape(ip, dshape_hat);

            Trans.SetIntPoint(&ip);
            CalcAdjugate(Trans.Jacobian(), Jadj);
            double w = ip.weight / Trans.Weight();

            Mult(dshape_hat, Jadj, dshape);
            dshape.GradToCurl(curlshape);

            if (VQ)
            {
                VQ->Eval(V, Trans, ip);
                V *= w;
                for (int j = 0; j < dim*dof; j++)
                {
                    crosscurlshape(j,0) = curlshape(j,1) * V(2) -
                                          curlshape(j,2) * V(1);
                    crosscurlshape(j,1) = curlshape(j,2) * V(0) -
                                          curlshape(j,0) * V(2);
                    crosscurlshape(j,2) = curlshape(j,0) * V(1) -
                                          curlshape(j,1) * V(0);
                }
                AddMultABt(crosscurlshape, curlshape, elmat);
            }
            else
            {
                AddMult_a_AAt(w, curlshape, elmat);
            }
        }
    };
};

} // end namespace mfem

#endif // MYINTEGRATORS_HPP
