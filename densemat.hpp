#include "mfem.hpp"
#include <complex>

#ifndef _DENSEMAT_HPP
#define _DENSEMAT_HPP

using namespace mfem;

#ifdef MFEM_USE_LAPACK
extern "C" void
dggev_(char* jobvl, char* jobvr, int* n,
    double* a, int* lda, double* b, int* ldb,
    double* alphar, double* alphai, double* beta,
    double* vl, int* ldvl, double* vr, int* ldvr,
    double* work, int* lwork, int* info);
#endif

DenseMatrix makeDenseFromSparse(SparseMatrix const & spmat)
{
    DenseMatrix densemat(spmat.Height(), spmat.Width());
    for (int row = 0; row < spmat.Height(); ++row)
     {
         for (int col = 0; col < spmat.Width(); ++col)
         {
             densemat(row, col) = spmat(row, col);
         }
     }
     return densemat;
}

class GeneralEigensystem
{
private:
    DenseMatrix A, B;
    int n;
    Vector alphaR, alphaI, beta;
    DenseMatrix VL, VR;
    Vector ev;
#ifdef MFEM_USE_LAPACK
    char jobvl, jobvr;
    double* work;
    int lwork, info;
#endif
public:
    GeneralEigensystem(DenseMatrix& A_, DenseMatrix& B_) : A(A_), B(B_),
        n(A.Width()), alphaR(n), alphaI(n), beta(n), VL(n, n), VR(n, n)
    {
        ev.SetDataAndSize(NULL, n);
#ifdef MFEM_USE_LAPACK
        jobvl = 'N';
        jobvr = 'V';
        lwork = -1;
        work = new double[1];
#endif
    }
    void Eval()
    {
#ifdef MFEM_USE_LAPACK
        dggev_(&jobvl, &jobvr, &n, A.GetData(), &n, B.GetData(), &n,
            alphaR.GetData(), alphaI.GetData(), beta.GetData(),
            VL.GetData(), &n, VR.GetData(), &n,
            work, &lwork, &info);
        lwork = work[0];
        if (work) delete[] work;
        work = new double[lwork];
        dggev_(&jobvl, &jobvr, &n, A.GetData(), &n, B.GetData(), &n,
            alphaR.GetData(), alphaI.GetData(), beta.GetData(),
            VL.GetData(), &n, VR.GetData(), &n,
            work, &lwork, &info);
        if (info != 0)
        {
            mfem::err << "GeneralEigensystem::Eval() : DGGEV error code: " << info << "\n";
            mfem_error();
        }
        // alphaR.Print_HYPRE(std::cout);
        // alphaI.Print_HYPRE(std::cout);
        // beta.Print_HYPRE(std::cout);
#else
        mfem_error("GeneralEigensystem::Eval() : Compiled without LAPACK.");
#endif
    }
    std::complex<double> eigenvalue(int i)
    {
        return std::complex<double>(alphaR(i)/beta(i), alphaI(i)/beta(i));
    }
    std::vector<std::complex<double>> eigenvector(int i)
    {
        std::vector<std::complex<double>> evector(n);
        if (alphaI(i) == 0.0)
        {
            for (int j = 0; j < n; ++j)
            {
                evector[j] = VR(j, i);
            }
        } else if (alphaI(i) > 0) {
            for (int j = 0; j < n; ++j)
            {
                evector[j] = (VR(j, i), VR(j, i+1));
            }
        } else {
            for (int j = 0; j < n; ++j)
            {
                evector[j] = (VR(j, i-1), VR(j, i));
            }
        }
        return evector;
    }
    Vector& eigenvector_component(int i)
    {
        ev.SetData(VR.Data() + i*VR.Height());
        return ev;
    }
    ~GeneralEigensystem()
    {
#ifdef MFEM_USE_LAPACK
        if (work) delete[] work;
#endif
    }
};

#endif // _DENSEMAT_HPP
