#!/usr/bin/python3
import argparse
import math
from scipy import integrate, interpolate, optimize
import numpy as np

preamble = """MFEM mesh v1.0

#
# MFEM Geometry Types (see mesh/geom.hpp):
#
# POINT       = 0
# SEGMENT     = 1
# TRIANGLE    = 2
# SQUARE      = 3
# TETRAHEDRON = 4
# CUBE        = 5
#

dimension
{}

# format: <attribute> <geometry type> <vertex 0> <vertex 1> ...
"""


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dimension', type=int, choices=[2, 3],
                        required=True,
                        help='Dimensionality of the box to generate.')
    parser.add_argument('-mx', default=3, type=int,
                        help='Number of elements in the x-direction.')
    parser.add_argument('-my', default=3, type=int,
                        help='Number of elements in the y-direction.')
    parser.add_argument('-mz', default=3, type=int,
                        help='Number of elements in the z-direction.')
    parser.add_argument('-xmin', default=-1.0, type=float,
                        help='Minimum value of the x-coordinate.')
    parser.add_argument('-xmax', default=1.0, type=float,
                        help='Maximum value of the x-coordinate.')
    parser.add_argument('-ymin', default=-1.0, type=float,
                        help='Minimum value of the y-coordinate.')
    parser.add_argument('-ymax', default=1.0, type=float,
                        help='Maximum value of the y-coordinate.')
    parser.add_argument('-zmin', default=-0.1, type=float,
                        help='Minimum value of the z-coordinate.')
    parser.add_argument('-zmax', default=0.1, type=float,
                        help='Maximum value of the z-coordinate.')
    parser.add_argument('-xpack', nargs='*', type=float,
                        help='X-coordinate of Gaussian for packing.')
    parser.add_argument('-amp', nargs='*', type=float,
                        help='Amplitude of Gaussian for packing.')
    parser.add_argument('-wpack', nargs='*', type=float,
                        help='Width of Gaussian for packing.')
    parser.add_argument('-p', '--periodicity', default='y-dir',
                        choices=['none', 'x-dir', 'y-dir', 'both'],
                        help='Periodicity of the box.')
    options = vars(parser.parse_args())
    if options['xpack'] is not None:
        if len(options['xpack']) != len(options['amp']):
            print('Length of amp and xpack do not match.')
            quit()
        if len(options['xpack']) != len(options['wpack']):
            print('Length of wpack and xpack do not match.')
            quit()
    return options


def weight_func(x, xlen, xpack, amp, wpack):
    weight = 1.0
    for i in range(len(xpack)):
        weight += math.exp(-math.pow((x - xpack[i]) / (xlen * wpack[i]), 2))
    return weight


def map_xsi_to_x(xsi_func, xsi_grid):
    xnew = np.zeros(xsi_grid.shape)
    for i in range(xsi_grid.size):
        def xsi_shift(x):
            return xsi_func(x) - xsi_grid[i]
        xnew[i] = optimize.fsolve(xsi_shift, 0.0)
    return xnew


def transform_dim(x, xpack, amp, wpack):
    weight = [weight_func(xval, x[-1] - x[0], xpack, amp, wpack) for xval in x]
    xsi = integrate.cumtrapz(weight, x=x, initial=0.0)
    xsi_func = interpolate.interp1d(x, xsi)
    xsi_grid = np.linspace(xsi[0], xsi[-1], num=xsi.size)
    return map_xsi_to_x(xsi_func, xsi_grid)


def make_2d_grid(options):
    x = np.linspace(options['xmin'], options['xmax'], num=options['mx']+1)
    y = np.linspace(options['ymin'], options['ymax'], num=options['my']+1)
    if options['xpack'] is not None:
        x = transform_dim(np.asarray(x), options['xpack'],
                          options['amp'], options['wpack'])
    return x, y


def write_2d_elements(mx, my, periodicity, file):
    file.write('elements\n')
    file.write(str(mx*my) + '\n')
    if periodicity == 'both' or periodicity == 'x-dir':
        nx = mx
    else:
        nx = mx+1
    for j in range(my-1):
        for i in range(mx-1):
            line = '1 3 {} {} {} {}\n'.format(
                nx*j+i, nx*j+i+1, nx*(j+1)+i+1, nx*(j+1)+i)
            file.write(line)
        if periodicity == 'both' or periodicity == 'x-dir':
            line = '1 3 {} {} {} {}\n'.format(
                    nx*j+mx-1, nx*j, nx*(j+1), nx*(j+1)+mx-1)
        else:
            line = '1 3 {} {} {} {}\n'.format(
                    nx*j+mx-1, nx*j+mx, nx*(j+1)+mx, nx*(j+1)+mx-1)
        file.write(line)
    for i in range(mx-1):
        if periodicity == 'both' or periodicity == 'y-dir':
            line = '1 3 {} {} {} {}\n'.format(
                    nx*(my-1)+i, nx*(my-1)+i+1, i+1, i)
        else:
            line = '1 3 {} {} {} {}\n'.format(
                    nx*(my-1)+i, nx*(my-1)+i+1, nx*my+i+1, nx*my+i)
        file.write(line)
    if periodicity == 'both':
        line = '1 3 {} {} {} {}\n'.format(nx*(my-1)+mx-1, nx*(my-1), 0, mx-1)
    elif periodicity == 'y-dir':
        line = '1 3 {} {} {} {}\n'.format(
                nx*(my-1)+mx-1, nx*(my-1)+mx, mx, mx-1)
    elif periodicity == 'x-dir':
        line = '1 3 {} {} {} {}\n'.format(
                nx*(my-1)+mx-1, nx*(my-1), nx*my, nx*my+mx-1)
    else:
        line = '1 3 {} {} {} {}\n'.format(
                nx*(my-1)+mx-1, nx*(my-1)+mx, nx*my+mx, nx*my+mx-1)
    file.write(line)
    file.write('\n')


def write_2d_boundary(mx, my, periodicity, file):
    file.write('boundary\n')
    if periodicity == 'both':
        file.write('0\n')
    elif periodicity == 'y-dir':
        nx = mx+1
        file.write('{}\n'.format(2*my))
        for j in range(my-1):
            line = '2 1 {1} {0}\n'.format(nx*j, nx*(j+1))
            file.write(line)
        line = '2 1 {1} {0}\n'.format(nx*(my-1), 0)
        file.write(line)
        for j in range(my-1):
            line = '2 1 {0} {1}\n'.format(nx*j+mx, nx*(j+1)+mx)
            file.write(line)
        line = '2 1 {0} {1}\n'.format(nx*(my-1)+mx, mx)
        file.write(line)
    elif periodicity == 'x-dir':
        nx = mx
        file.write('{}\n'.format(2*mx))
        for i in range(mx-1):
            line = '2 1 {0} {1}\n'.format(i, i+1)
            file.write(line)
        line = '2 1 {0} {1}\n'.format(mx-1, 0)
        file.write(line)
        for i in range(mx-1):
            line = '2 1 {1} {0}\n'.format(nx*my+i, nx*my+i+1)
            file.write(line)
        line = '2 1 {1} {0}\n'.format(nx*my+mx-1, nx*my)
        file.write(line)
    else:
        nx = mx+1
        file.write('{}\n'.format(2*(mx+my)))
        for i in range(mx):
            line = '2 1 {0} {1}\n'.format(i, i+1)
            file.write(line)
        for i in range(mx):
            line = '2 1 {1} {0}\n'.format(nx*my+i, nx*my+i+1)
            file.write(line)
        for j in range(my):
            line = '2 1 {1} {0}\n'.format(nx*j, nx*(j+1))
            file.write(line)
        for j in range(my):
            line = '2 1 {0} {1}\n'.format(nx*j+mx, nx*(j+1)+mx)
            file.write(line)
    file.write('\n')


def write_2d_nodes(x, y, file):
    for j in range(y.size-1):
        for i in range(x.size-1):
            node0 = '{:.10} {:.10}\n'.format(x[i], y[j])
            node1 = '{:.10} {:.10}\n'.format(x[i+1], y[j])
            node2 = '{:.10} {:.10}\n'.format(x[i], y[j+1])
            node3 = '{:.10} {:.10}\n'.format(x[i+1], y[j+1])
            file.write(node0 + node1 + node2 + node3 + '\n')


def write_2d_mesh(x, y, periodicity, filename):
    with open(filename, 'w') as f:
        f.write(preamble.format(2))
        write_2d_elements(x.size-1, y.size-1, periodicity, f)
        write_2d_boundary(x.size-1, y.size-1, periodicity, f)
        if periodicity == 'both':
            nvert = (x.size-1)*(y.size-1)
        elif periodicity == 'y-dir':
            nvert = x.size*(y.size-1)
        elif periodicity == 'x-dir':
            nvert = (x.size-1)*y.size
        else:
            nvert = x.size*y.size
        f.write('vertices\n' + str(nvert) + '\n\n')
        f.write('nodes\n'
                'FiniteElementSpace\n'
                'FiniteElementCollection: L2_T1_2D_P1\n'
                'VDim: 2\n'
                'Ordering: 1\n\n')
        write_2d_nodes(x, y, f)


def make_3d_grid(options):
    x = np.linspace(options['xmin'], options['xmax'], num=options['mx']+1)
    y = np.linspace(options['ymin'], options['ymax'], num=options['my']+1)
    z = np.linspace(options['zmin'], options['zmax'], num=options['mz']+1)
    if options['xpack'] is not None:
        x = transform_dim(np.asarray(x), options['xpack'],
                          options['amp'], options['wpack'])
    return x, y, z


def write_3d_elements(mx, my, mz, periodicity, file):
    file.write('elements\n')
    file.write(str(mx*my*mz) + '\n')
    if periodicity == 'both':
        nx = mx
        nxy = nx*my
    elif periodicity == 'y-dir':
        nx = mx+1
        nxy = nx*my
    elif periodicity == 'x-dir':
        nx = mx
        nxy = nx*(my+1)
    else:
        nx = mx+1
        nxy = nx*(my+1)
    for k in range(mz-1):
        for j in range(my-1):
            for i in range(mx-1):
                line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                        nxy*k+nx*j+i, nxy*k+nx*j+i+1,
                        nxy*k+nx*(j+1)+i+1, nxy*k+nx*(j+1)+i,
                        nxy*(k+1)+nx*j+i, nxy*(k+1)+nx*j+i+1,
                        nxy*(k+1)+nx*(j+1)+i+1, nxy*(k+1)+nx*(j+1)+i)
                file.write(line)
            if periodicity == 'both' or periodicity == 'x-dir':
                line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                        nxy*k+nx*j+mx-1, nxy*k+nx*j,
                        nxy*k+nx*(j+1), nxy*k+nx*(j+1)+mx-1,
                        nxy*(k+1)+nx*j+mx-1, nxy*(k+1)+nx*j,
                        nxy*(k+1)+nx*(j+1), nxy*(k+1)+nx*(j+1)+mx-1)
            else:
                line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                        nxy*k+nx*j+mx-1, nxy*k+nx*j+mx,
                        nxy*k+nx*(j+1)+mx, nxy*k+nx*(j+1)+mx-1,
                        nxy*(k+1)+nx*j+mx-1, nxy*(k+1)+nx*j+mx,
                        nxy*(k+1)+nx*(j+1)+mx, nxy*(k+1)+nx*(j+1)+mx-1)
            file.write(line)
        for i in range(mx-1):
            if periodicity == 'both' or periodicity == 'y-dir':
                line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                        nxy*k+nx*(my-1)+i, nxy*k+nx*(my-1)+i+1,
                        nxy*k+i+1, nxy*k+i,
                        nxy*(k+1)+nx*(my-1)+i, nxy*(k+1)+nx*(my-1)+i+1,
                        nxy*(k+1)+i+1, nxy*(k+1)+i)
            else:
                line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                        nxy*k+nx*(my-1)+i, nxy*k+nx*(my-1)+i+1,
                        nxy*k+nx*my+i+1, nxy*k+nx*my+i,
                        nxy*(k+1)+nx*(my-1)+i, nxy*(k+1)+nx*(my-1)+i+1,
                        nxy*(k+1)+nx*my+i+1, nxy*(k+1)+nx*my+i)
            file.write(line)
        if periodicity == 'both':
            line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                    nxy*k+nx*(my-1)+mx-1, nxy*k+nx*(my-1),
                    nxy*k, nxy*k+mx-1,
                    nxy*(k+1)+nx*(my-1)+mx-1, nxy*(k+1)+nx*(my-1),
                    nxy*(k+1), nxy*(k+1)+mx-1)
        elif periodicity == 'y-dir':
            line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                    nxy*k+nx*(my-1)+mx-1, nxy*k+nx*(my-1)+mx,
                    nxy*k+mx, nxy*k+mx-1,
                    nxy*(k+1)+nx*(my-1)+mx-1, nxy*(k+1)+nx*(my-1)+mx,
                    nxy*(k+1)+mx, nxy*(k+1)+mx-1)
        elif periodicity == 'x-dir':
            line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                    nxy*k+nx*(my-1)+mx-1, nxy*k+nx*(my-1),
                    nxy*k+nx*my, nxy*k+nx*my+mx-1,
                    nxy*(k+1)+nx*(my-1)+mx-1, nxy*(k+1)+nx*(my-1),
                    nxy*(k+1)+nx*my, nxy*(k+1)+nx*my+mx-1)
        else:
            line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                    nxy*k+nx*(my-1)+mx-1, nxy*k+nx*(my-1)+mx,
                    nxy*k+nx*my+mx, nxy*k+nx*my+mx-1,
                    nxy*(k+1)+nx*(my-1)+mx-1, nxy*(k+1)+nx*(my-1)+mx,
                    nxy*(k+1)+nx*my+mx, nxy*(k+1)+nx*my+mx-1)
        file.write(line)
    for j in range(my-1):
        for i in range(mx-1):
            line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                    nxy*(mz-1)+nx*j+i, nxy*(mz-1)+nx*j+i+1,
                    nxy*(mz-1)+nx*(j+1)+i+1, nxy*(mz-1)+nx*(j+1)+i,
                    nx*j+i, nx*j+i+1,
                    nx*(j+1)+i+1, nx*(j+1)+i)
            file.write(line)
        if periodicity == 'both' or periodicity == 'x-dir':
            line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                    nxy*(mz-1)+nx*j+mx-1, nxy*(mz-1)+nx*j,
                    nxy*(mz-1)+nx*(j+1), nxy*(mz-1)+nx*(j+1)+mx-1,
                    nx*j+mx-1, nx*j,
                    nx*(j+1), nx*(j+1)+mx-1)
        else:
            line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                    nxy*(mz-1)+nx*j+mx-1, nxy*(mz-1)+nx*j+mx,
                    nxy*(mz-1)+nx*(j+1)+mx, nxy*(mz-1)+nx*(j+1)+mx-1,
                    nx*j+mx-1, nx*j+mx,
                    nx*(j+1)+mx, nx*(j+1)+mx-1)
        file.write(line)
    for i in range(mx-1):
        if periodicity == 'both' or periodicity == 'y-dir':
            line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                    nxy*(mz-1)+nx*(my-1)+i, nxy*(mz-1)+nx*(my-1)+i+1,
                    nxy*(mz-1)+i+1, nxy*(mz-1)+i,
                    nx*(my-1)+i, nx*(my-1)+i+1,
                    i+1, i)
        else:
            line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                    nxy*(mz-1)+nx*(my-1)+i, nxy*(mz-1)+nx*(my-1)+i+1,
                    nxy*(mz-1)+nx*my+i+1, nxy*(mz-1)+nx*my+i,
                    nx*(my-1)+i, nx*(my-1)+i+1,
                    nx*my+i+1, nx*my+i)
        file.write(line)
    if periodicity == 'both':
        line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                nxy*(mz-1)+nx*(my-1)+mx-1, nxy*(mz-1)+nx*(my-1),
                nxy*(mz-1), nxy*(mz-1)+mx-1,
                nx*(my-1)+mx-1, nx*(my-1),
                0, mx-1)
    elif periodicity == 'y-dir':
        line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                nxy*(mz-1)+nx*(my-1)+mx-1, nxy*(mz-1)+nx*(my-1)+mx,
                nxy*(mz-1)+mx, nxy*(mz-1)+mx-1,
                nx*(my-1)+mx-1, nx*(my-1)+mx,
                mx, mx-1)
    elif periodicity == 'x-dir':
        line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                nxy*(mz-1)+nx*(my-1)+mx-1, nxy*(mz-1)+nx*(my-1),
                nxy*(mz-1)+nx*my, nxy*(mz-1)+nx*my+mx-1,
                nx*(my-1)+mx-1, nx*(my-1),
                nx*my, nx*my+mx-1)
    else:
        line = '1 5 {} {} {} {} {} {} {} {}\n'.format(
                nxy*(mz-1)+nx*(my-1)+mx-1, nxy*(mz-1)+nx*(my-1)+mx,
                nxy*(mz-1)+nx*my+mx, nxy*(mz-1)+nx*my+mx-1,
                nx*(my-1)+mx-1, nx*(my-1)+mx,
                nx*my+mx, nx*my+mx-1)
    file.write(line)
    file.write('\n')


def write_3d_boundary(mx, my, mz, periodicity, file):
    file.write('boundary\n')
    file.write('{}\n'.format(2*(mx*my+my*mz+mz*mx)))
    if periodicity == 'both':
        nx = mx
        nxy = mx*my
        for j in range(my-1):
            for i in range(mx-1):
                line = '1 3 {0} {1} {3} {2}\n'.format(
                        nx*j+i, nx*j+i+1,
                        nx*(j+1)+i, nx*(j+1)+i+1)
                file.write(line)
            line = '1 3 {0} {1} {3} {2}\n'.format(
                    nx*j+mx-1, nx*j,
                    nx*(j+1)+mx-1, nx*(j+1))
            file.write(line)
        for i in range(mx-1):
            line = '1 3 {0} {1} {3} {2}\n'.format(
                    nx*(my-1)+i, nx*(my-1)+i+1,
                    i, i+1)
            file.write(line)
        line = '1 3 {0} {1} {3} {2}\n'.format(
                nx*(my-1)+mx-1, nx*(my-1),
                mx-1, 0)
        file.write(line)
        for j in range(my-1):
            for i in range(mx-1):
                line = '1 3 {3} {1} {0} {2}\n'.format(
                        nx*j+i, nx*j+i+1,
                        nx*(j+1)+i, nx*(j+1)+i+1)
                file.write(line)
            line = '1 3 {3} {1} {0} {2}\n'.format(
                    nx*j+mx-1, nx*j,
                    nx*(j+1)+mx-1, nx*(j+1))
            file.write(line)
        for i in range(mx-1):
            line = '1 3 {3} {1} {0} {2}\n'.format(
                    nx*(my-1)+i, nx*(my-1)+i+1,
                    i, i+1)
            file.write(line)
        line = '1 3 {3} {1} {0} {2}\n'.format(
                nx*(my-1)+mx-1, nx*(my-1),
                mx-1, 0)
        file.write(line)
        for k in range(mz-1):
            for j in range(my-1):
                line = '1 3 {0} {1} {3} {2}\n'.format(
                        nxy*k+nx*j, nxy*k+nx*(j+1),
                        nxy*(k+1)+nx*j, nxy*(k+1)+nx*(j+1))
                file.write(line)
            line = '1 3 {0} {1} {3} {2}\n'.format(
                    nxy*k+nx*(my-1), nxy*k,
                    nxy*(k+1)+nx*(my-1), nxy*(k+1))
            file.write(line)
        for j in range(my-1):
            line = '1 3 {0} {1} {3} {2}\n'.format(
                    nxy*(mz-1)+nx*j, nxy*(mz-1)+nx*(j+1),
                    nx*j, nx*(j+1))
            file.write(line)
        line = '1 3 {0} {1} {3} {2}\n'.format(
                nxy*(mz-1)+nx*(my-1), nxy*(mz-1),
                nx*(my-1), 0)
        file.write(line)
        for k in range(mz-1):
            for j in range(my-1):
                line = '1 3 {3} {1} {0} {2}\n'.format(
                        nxy*k+nx*j, nxy*k+nx*(j+1),
                        nxy*(k+1)+nx*j, nxy*(k+1)+nx*(j+1))
                file.write(line)
            line = '1 3 {3} {1} {0} {2}\n'.format(
                    nxy*k+nx*(my-1), nxy*k,
                    nxy*(k+1)+nx*(my-1), nxy*(k+1))
            file.write(line)
        for j in range(my-1):
            line = '1 3 {3} {1} {0} {2}\n'.format(
                    nxy*(mz-1)+nx*j, nxy*(mz-1)+nx*(j+1),
                    nx*j, nx*(j+1))
            file.write(line)
        line = '1 3 {3} {1} {0} {2}\n'.format(
                nxy*(mz-1)+nx*(my-1), nxy*(mz-1),
                nx*(my-1), 0)
        file.write(line)
        for k in range(mz-1):
            for i in range(mx-1):
                line = '1 3 {0} {1} {3} {2}\n'.format(
                        nxy*k+i, nxy*k+i+1,
                        nxy*(k+1)+i, nxy*(k+1)+i+1)
                file.write(line)
            line = '1 3 {0} {1} {3} {2}\n'.format(
                    nxy*k+mx-1, nxy*k,
                    nxy*(k+1)+mx-1, nxy*(k+1))
            file.write(line)
        for i in range(mx-1):
            line = '1 3 {0} {1} {3} {2}\n'.format(
                    nxy*(mz-1)+i, nxy*(mz-1)+i+1,
                    i, i+1)
            file.write(line)
        line = '1 3 {0} {1} {3} {2}\n'.format(
                nxy*(mz-1)+mx-1, nxy*(mz-1),
                mx-1, 0)
        file.write(line)
        for k in range(mz-1):
            for i in range(mx-1):
                line = '1 3 {3} {1} {0} {2}\n'.format(
                        nxy*k+i, nxy*k+i+1,
                        nxy*(k+1)+i, nxy*(k+1)+i+1)
                file.write(line)
            line = '1 3 {3} {1} {0} {2}\n'.format(
                    nxy*k++mx-1, nxy*k,
                    nxy*(k+1)+mx-1, nxy*(k+1))
            file.write(line)
        for i in range(mx-1):
            line = '1 3 {3} {1} {0} {2}\n'.format(
                    nxy*(mz-1)+i, nxy*(mz-1)+i+1,
                    i, i+1)
            file.write(line)
        line = '1 3 {3} {1} {0} {2}\n'.format(
                nxy*(mz-1)+mx-1, nxy*(mz-1),
                mx-1, 0)
        file.write(line)
    elif periodicity == 'y-dir':
        nx = mx+1
        nxy = nx*my
        for j in range(my-1):
            for i in range(mx):
                line = '1 3 {0} {1} {3} {2}\n'.format(
                        nx*j+i, nx*j+i+1,
                        nx*(j+1)+i, nx*(j+1)+i+1)
                file.write(line)
        for i in range(mx):
            line = '1 3 {0} {1} {3} {2}\n'.format(
                    nx*(my-1)+i, nx*(my-1)+i+1,
                    i, i+1)
            file.write(line)
        for j in range(my-1):
            for i in range(mx):
                line = '1 3 {3} {1} {0} {2}\n'.format(
                        nx*j+i, nx*j+i+1,
                        nx*(j+1)+i, nx*(j+1)+i+1)
                file.write(line)
        for i in range(mx):
            line = '1 3 {3} {1} {0} {2}\n'.format(
                    nx*(my-1)+i, nx*(my-1)+i+1,
                    i, i+1)
            file.write(line)
        for k in range(mz-1):
            for j in range(my-1):
                line = '2 3 {0} {1} {3} {2}\n'.format(
                        nxy*k+nx*j, nxy*k+nx*(j+1),
                        nxy*(k+1)+nx*j, nxy*(k+1)+nx*(j+1))
                file.write(line)
            line = '2 3 {0} {1} {3} {2}\n'.format(
                    nxy*k+nx*(my-1), nxy*k,
                    nxy*(k+1)+nx*(my-1), nxy*(k+1))
            file.write(line)
        for j in range(my-1):
            line = '2 3 {0} {1} {3} {2}\n'.format(
                    nxy*(mz-1)+nx*j, nxy*(mz-1)+nx*(j+1),
                    nx*j, nx*(j+1))
            file.write(line)
        line = '2 3 {0} {1} {3} {2}\n'.format(
                nxy*(mz-1)+nx*(my-1), nxy*(mz-1),
                nx*(my-1), 0)
        file.write(line)
        for k in range(mz-1):
            for j in range(my-1):
                line = '2 3 {3} {1} {0} {2}\n'.format(
                        nxy*k+nx*j+mx, nxy*k+nx*(j+1)+mx,
                        nxy*(k+1)+nx*j+mx, nxy*(k+1)+nx*(j+1)+mx)
                file.write(line)
            line = '2 3 {3} {1} {0} {2}\n'.format(
                    nxy*k+nx*(my-1)+mx, nxy*k+mx,
                    nxy*(k+1)+nx*(my-1)+mx, nxy*(k+1)+mx)
            file.write(line)
        for j in range(my-1):
            line = '2 3 {3} {1} {0} {2}\n'.format(
                    nxy*(mz-1)+nx*j+mx, nxy*(mz-1)+nx*(j+1)+mx,
                    nx*j+mx, nx*(j+1)+mx)
            file.write(line)
        line = '2 3 {3} {1} {0} {2}\n'.format(
                nxy*(mz-1)+nx*(my-1)+mx, nxy*(mz-1)+mx,
                nx*(my-1)+mx, mx)
        file.write(line)
        for k in range(mz-1):
            for i in range(mx):
                line = '1 3 {0} {1} {3} {2}\n'.format(
                        nxy*k+i, nxy*k+i+1,
                        nxy*(k+1)+i, nxy*(k+1)+i+1)
                file.write(line)
        for i in range(mx):
            line = '1 3 {0} {1} {3} {2}\n'.format(
                    nxy*(mz-1)+i, nxy*(mz-1)+i+1,
                    i, i+1)
            file.write(line)
        for k in range(mz-1):
            for i in range(mx):
                line = '1 3 {3} {1} {0} {2}\n'.format(
                        nxy*k+i, nxy*k+i+1,
                        nxy*(k+1)+i, nxy*(k+1)+i+1)
                file.write(line)
        for i in range(mx):
            line = '1 3 {3} {1} {0} {2}\n'.format(
                    nxy*(mz-1)+i, nxy*(mz-1)+i+1,
                    i, i+1)
            file.write(line)
    elif periodicity == 'x-dir':
        nx = mx
        nxy = nx*(my+1)
        for j in range(my):
            for i in range(mx-1):
                line = '1 3 {0} {1} {3} {2}\n'.format(
                        nx*j+i, nx*j+i+1,
                        nx*(j+1)+i, nx*(j+1)+i+1)
                file.write(line)
            line = '1 3 {0} {1} {3} {2}\n'.format(
                    nx*j+mx-1, nx*j,
                    nx*(j+1)+mx-1, nx*(j+1))
            file.write(line)
        for j in range(my):
            for i in range(mx-1):
                line = '1 3 {3} {1} {0} {2}\n'.format(
                        nx*j+i, nx*j+i+1,
                        nx*(j+1)+i, nx*(j+1)+i+1)
                file.write(line)
            line = '1 3 {3} {1} {0} {2}\n'.format(
                    nx*j+mx-1, nx*j,
                    nx*(j+1)+mx-1, nx*(j+1))
            file.write(line)
        for k in range(mz-1):
            for j in range(my):
                line = '1 3 {0} {1} {3} {2}\n'.format(
                        nxy*k+nx*j, nxy*k+nx*(j+1),
                        nxy*(k+1)+nx*j, nxy*(k+1)+nx*(j+1))
                file.write(line)
        for j in range(my):
            line = '1 3 {0} {1} {3} {2}\n'.format(
                    nxy*(mz-1)+nx*j, nxy*(mz-1)+nx*(j+1),
                    nx*j, nx*(j+1))
            file.write(line)
        for k in range(mz-1):
            for j in range(my):
                line = '1 3 {3} {1} {0} {2}\n'.format(
                        nxy*k+nx*j, nxy*k+nx*(j+1),
                        nxy*(k+1)+nx*j, nxy*(k+1)+nx*(j+1))
                file.write(line)
        for j in range(my):
            line = '1 3 {3} {1} {0} {2}\n'.format(
                    nxy*(mz-1)+nx*j, nxy*(mz-1)+nx*(j+1),
                    nx*j, nx*(j+1))
            file.write(line)
        for k in range(mz-1):
            for i in range(mx-1):
                line = '2 3 {0} {1} {3} {2}\n'.format(
                        nxy*k+i, nxy*k+i+1,
                        nxy*(k+1)+i, nxy*(k+1)+i+1)
                file.write(line)
            line = '2 3 {0} {1} {3} {2}\n'.format(
                    nxy*k+mx-1, nxy*k,
                    nxy*(k+1)+mx-1, nxy*(k+1))
            file.write(line)
        for i in range(mx-1):
            line = '2 3 {0} {1} {3} {2}\n'.format(
                    nxy*(mz-1)+i, nxy*(mz-1)+i+1,
                    i, i+1)
            file.write(line)
        line = '2 3 {0} {1} {3} {2}\n'.format(
                nxy*(mz-1)+mx-1, nxy*(mz-1),
                mx-1, 0)
        file.write(line)
        for k in range(mz-1):
            for i in range(mx-1):
                line = '2 3 {3} {1} {0} {2}\n'.format(
                        nxy*k+nx*my+i, nxy*k+nx*my+i+1,
                        nxy*(k+1)+nx*my+i, nxy*(k+1)+nx*my+i+1)
                file.write(line)
            line = '2 3 {3} {1} {0} {2}\n'.format(
                    nxy*k+nx*my+mx-1, nxy*k+nx*my,
                    nxy*(k+1)+nx*my+mx-1, nxy*(k+1)+nx*my)
            file.write(line)
        for i in range(mx-1):
            line = '2 3 {3} {1} {0} {2}\n'.format(
                    nxy*(mz-1)+nx*my+i, nxy*(mz-1)+nx*my+i+1,
                    nx*my+i, nx*my+i+1)
            file.write(line)
        line = '2 3 {3} {1} {0} {2}\n'.format(
                nxy*(mz-1)+nx*my+mx-1, nxy*(mz-1)+nx*my,
                nx*my+mx-1, nx*my)
        file.write(line)
    else:
        nx = mx+1
        nxy = nx*(my+1)
        for j in range(my):
            for i in range(mx):
                line = '1 3 {0} {1} {3} {2}\n'.format(
                        nx*j+i, nx*j+i+1,
                        nx*(j+1)+i, nx*(j+1)+i+1)
                file.write(line)
        for j in range(my):
            for i in range(mx):
                line = '1 3 {3} {1} {0} {2}\n'.format(
                        nx*j+i, nx*j+i+1,
                        nx*(j+1)+i, nx*(j+1)+i+1)
                file.write(line)
        for k in range(mz-1):
            for j in range(my):
                line = '2 3 {0} {1} {3} {2}\n'.format(
                        nxy*k+nx*j, nxy*k+nx*(j+1),
                        nxy*(k+1)+nx*j, nxy*(k+1)+nx*(j+1))
                file.write(line)
        for j in range(my):
            line = '2 3 {0} {1} {3} {2}\n'.format(
                    nxy*(mz-1)+nx*j, nxy*(mz-1)+nx*(j+1),
                    nx*j, nx*(j+1))
            file.write(line)
        for k in range(mz-1):
            for j in range(my):
                line = '2 3 {3} {1} {0} {2}\n'.format(
                        nxy*k+nx*j+mx, nxy*k+nx*(j+1)+mx,
                        nxy*(k+1)+nx*j+mx, nxy*(k+1)+nx*(j+1)+mx)
                file.write(line)
        for j in range(my):
            line = '2 3 {3} {1} {0} {2}\n'.format(
                    nxy*(mz-1)+nx*j+mx, nxy*(mz-1)+nx*(j+1)+mx,
                    nx*j+mx, nx*(j+1)+mx)
            file.write(line)
        for k in range(mz-1):
            for i in range(mx):
                line = '2 3 {0} {1} {3} {2}\n'.format(
                        nxy*k+i, nxy*k+i+1,
                        nxy*(k+1)+i, nxy*(k+1)+i+1)
                file.write(line)
        for i in range(mx):
            line = '2 3 {0} {1} {3} {2}\n'.format(
                    nxy*(mz-1)+i, nxy*(mz-1)+i+1,
                    i, i+1)
            file.write(line)
        for k in range(mz-1):
            for i in range(mx):
                line = '2 3 {3} {1} {0} {2}\n'.format(
                        nxy*k+nx*my+i, nxy*k+nx*my+i+1,
                        nxy*(k+1)+nx*my+i, nxy*(k+1)+nx*my+i+1)
                file.write(line)
        for i in range(mx):
            line = '2 3 {3} {1} {0} {2}\n'.format(
                    nxy*(mz-1)+nx*my+i, nxy*(mz-1)+nx*my+i+1,
                    nx*my+i, nx*my+i+1)
            file.write(line)
    file.write('\n')


def write_3d_nodes(x, y, z, file):
    for k in range(z.size-1):
        for j in range(y.size-1):
            for i in range(x.size-1):
                node0 = '{:.10} {:.10} {:.10}\n'.format(x[i], y[j], z[k])
                node1 = '{:.10} {:.10} {:.10}\n'.format(x[i+1], y[j], z[k])
                node2 = '{:.10} {:.10} {:.10}\n'.format(x[i], y[j+1], z[k])
                node3 = '{:.10} {:.10} {:.10}\n'.format(x[i+1], y[j+1], z[k])
                node4 = '{:.10} {:.10} {:.10}\n'.format(x[i], y[j], z[k+1])
                node5 = '{:.10} {:.10} {:.10}\n'.format(x[i+1], y[j], z[k+1])
                node6 = '{:.10} {:.10} {:.10}\n'.format(x[i], y[j+1], z[k+1])
                node7 = '{:.10} {:.10} {:.10}\n'.format(x[i+1], y[j+1], z[k+1])
                file.write(node0 + node1 + node2 + node3
                           + node4 + node5 + node6 + node7 + '\n')


def write_3d_mesh(x, y, z, periodicity, filename):
    with open(filename, 'w') as f:
        f.write(preamble.format(3))
        write_3d_elements(x.size-1, y.size-1, z.size-1, periodicity, f)
        write_3d_boundary(x.size-1, y.size-1, z.size-1, periodicity, f)
        if periodicity == 'both':
            nvert = (x.size-1)*(y.size-1)*(z.size-1)
        elif periodicity == 'y-dir':
            nvert = x.size*(y.size-1)*(z.size-1)
        elif periodicity == 'x-dir':
            nvert = (x.size-1)*y.size*(z.size-1)
        else:
            nvert = x.size*y.size*(z.size-1)
        f.write('vertices\n' + str(nvert) + '\n\n')
        f.write('nodes\n'
                'FiniteElementSpace\n'
                'FiniteElementCollection: L2_T1_3D_P1\n'
                'VDim: 3\n'
                'Ordering: 1\n\n')
        write_3d_nodes(x, y, z, f)


def main():
    options = get_options()
    print(options)
    if options['dimension'] == 2:
        x, y = make_2d_grid(options)
        if options['xpack'] is not None:
            filename = 'periodic-square-packed.mesh'
        else:
            filename = 'periodic-square.mesh'
        write_2d_mesh(x, y, options['periodicity'], filename)
    elif options['dimension'] == 3:
        x, y, z = make_3d_grid(options)
        if options['xpack'] is not None:
            filename = 'periodic-cube-packed.mesh'
        else:
            filename = 'periodic-cube.mesh'
        write_3d_mesh(x, y, z, options['periodicity'], filename)


if __name__ == '__main__':
    main()
