/*
                    Helicon/Whistler Wave Evolution MiniApp

    Compile with:   make helicon

    Description:    This code solve the time-dependent electron MHD equation
                    db/dt = - curl(curl(b) x B), where B is the background
                    magnetic field (normalized by mu_0*n*e).

    Time Advance:
                    Implicit options:
                      L-stable
                        0. Backward Euler
                        1. SDIRK 33
                      A-stable
                        2. Crank-Nicolson
                        3. SDIRK 34

    Variational Forms:
        0.  Find b in H(curl) such that
                <db/dt, c> = -<B x curl(b), curl(c)>    for all c in H(curl)
        1.  Find b in H1 such that
                <db/dt, c> = -<B x curl(b), curl(c)> + k*<div(b), div(c)>
                                                        for all c in H1
        2.  Find b,e in {H1,H(curl)} such that
                <db/dt, c> = -<curl(e), c> + k*<div(b), div(c)>
                                                        for all c in H1
                    <e, d> = -<B x curl(b), d>          for all d in H(curl)
        3. Find b,e,j in {H(div),H(curl),H(curl)} such that
                <db/dt, c> = -<curl(e), c>              for all c in H(div)
                    <e, d> = -<B x j, d>                for all d in H(curl)
                    <j, h> = <b, curl(h)>               for all h in H(curl)
        4. Find b,j in {H1,H1} such that
                to be figured out
        5. Find b,e in {H1,H(curl)} such that
                to be figured out
 */
#include "myintegrators.hpp"
#include "mfem.hpp"
#include <cmath>
#include <iostream>
#include <fstream>

const double pi = std::acos(-1);

/* Make proc number and id global. */
int num_procs, myid;

/* Choice for the problem setup. The background magnetic field and initial
 * condition are chosen based on this parameter. */
int problem;
int ny, nz;
double n;
double alpha;

// Background magnetic field function
void b0field_function(const mfem::Vector &x, mfem::Vector &b);
void binit_function(const mfem::Vector &x, mfem::Vector &b);

// Mesh bounding box
mfem::Vector bb_min, bb_max;

/* A time-dependent oprator for the right-hand side of the ODE. The weak form of
 * db/dt = curl(curl(b) x B) is M db/dt = K b, where M and K are the mass and
 * CrossCurlCurl matrices, respectively. We are only considering implicit time-
 * advance schemes so we will solve the ODE, db/dt = (M - dt K)^{-1} (K b). */
class Galerkin : public mfem::TimeDependentOperator
{
private:
    mfem::HypreParMatrix &M, &K;
    mfem::HypreParMatrix *T; // T = M - dt K
    mfem::HypreSmoother T_prec;
    mfem::GMRESSolver T_solver;
    double current_dt;

    mutable mfem::Vector z;

public:
    Galerkin(mfem::HypreParMatrix &_M, mfem::HypreParMatrix &_K);

    virtual void ImplicitSolve(const double dt, const mfem::Vector &x,
        mfem::Vector &b);

    virtual ~Galerkin();
};

int main(int argc, char *argv[])
{
    // 1. Initialize MPI.
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);

    // 2. Parse command-line options.
    problem = 0;
    const char *mesh_file = "periodic-cube.mesh";
    int ser_ref_levels = 1;
    int par_ref_levels = 0;
    int order = 2;
    int ode_solver_type = 0;
    int formulation = 0;
    double t_final = 10.0;
    double dt = 0.01;
    bool visualization = true;
    int vis_steps = 5;
    ny = 0;
    nz = 1;
    double kdivb_in = 1.0;
    alpha = 0.0;

    int precision = 8;
    std::cout.precision(precision);

    mfem::OptionsParser args(argc, argv);
    args.AddOption(&mesh_file, "-m", "--mesh",
                   "Mesh file to use.");
    args.AddOption(&problem, "-p", "--problem",
                   "Problem setup to use. See options in b0field_function().");
    args.AddOption(&ser_ref_levels, "-rs", "--refine-serial",
                   "Number of times to refine the mesh uniformly in serial.");
    args.AddOption(&par_ref_levels, "-rp", "--refine-parallel",
                   "Number of times to refine the mesh uniformly in parallel.");
    args.AddOption(&order, "-o", "--order",
                   "Order (degree) of the finite elements.");
    args.AddOption(&ode_solver_type, "-s", "--ode-solver",
                   "ODE solver:\n\t"
                   "    Implicit: 0 - Backward Euler, 1 - SDIRK 33,"
                                " 2 - Crank-Nicolson, 3 - SDIRK 34");
    args.AddOption(&formulation, "-f", "--formulation",
                   "Choose which variational form to use.\n\t"
                   "0-1 - Galerkin, 2-3 - Mixed, 4-5 - FOSLS.");
    args.AddOption(&t_final, "-tf", "--t-final",
                   "Final time; start time is 0.");
    args.AddOption(&dt, "-dt", "--time-step",
                   "Time step.");
    args.AddOption(&visualization, "-vis", "--visualization", "-no-vis",
                   "--no-visualization",
                   "Enable or disable GLVis visualization.");
    args.AddOption(&vis_steps, "-vs", "--visualization-steps",
                   "Visualize every n-th timestep.");
    args.AddOption(&ny, "-ny", "--ny", "Number of periods in y.");
    args.AddOption(&nz, "-nz", "--nz", "Number of periods in z.");
    args.AddOption(&kdivb_in, "-k", "--k-divb",
                   "Divergence cleaner constant.");
    args.AddOption(&alpha, "-a", "--alpha",
                   "Fraction of pi for angle of magnetic field.");
    args.Parse();
    if (!args.Good())
    {
        if (myid == 0)
        {
            args.PrintUsage(std::cout);
        }
        MPI_Finalize();
        return 1;
    }
    if (myid == 0)
    {
        args.PrintOptions(std::cout);
    }
    n = std::sqrt(ny*ny + nz*nz);

    /* 3. Read the serial mesh from the given mesh file on all processors. We
     *    can handle geometrically periodic meshes in this code. */
    mfem::Mesh *mesh = new mfem::Mesh(mesh_file, 1, 1);
    int dim = mesh->Dimension();

    /* 4. Define the ODE solver used for time integration. Both explicit and
     *    implicit methods are available. */
    mfem::ODESolver *ode_solver = nullptr;
    switch (ode_solver_type)
    {
        case 0: ode_solver = new mfem::BackwardEulerSolver; break;
        case 1: ode_solver = new mfem::SDIRK33Solver; break;
        case 2: ode_solver = new mfem::ImplicitMidpointSolver; break;
        case 3: ode_solver = new mfem::SDIRK34Solver; break;
        default:
            if (myid == 0 )
            {
                std::cout << "Unknown ODE solver type: " << ode_solver_type
                          << std::endl;
            }
            delete mesh;
            MPI_Finalize();
            return 3;
    }

    /* 5. Refine the mesh in serial to increase the resolution. In this example
     *    we do 'ser_ref_levels' of uniform refinement, where 'ser_ref_levels'
     *    is a command-line parameter. If the mesh is of NURBS type, we convert
     *    it to a (piecewise-polynomial) high-order mesh. */
    for (int lev = 0; lev < ser_ref_levels; lev++)
    {
        mesh->UniformRefinement();
    }
    mesh->GetBoundingBox(bb_min, bb_max, std::max(order, 1));

    /* 6. Define the parallel mesh by a partitioning of the serial mesh. Refine
     *    this mesh further in parallel to increase the resolution. Once the
     *    parallel mesh is defined, the serial mesh can be deleted. */
    mfem::ParMesh *pmesh = new mfem::ParMesh(MPI_COMM_WORLD, *mesh);
    delete mesh;
    for (int lev = 0; lev < par_ref_levels; lev++)
    {
        pmesh->UniformRefinement();
    }

    /* 7. Define the parallel H(curl) finite element space on the parallel
     *    refined mesh for the given polynomial order. */
    mfem::RT_FECollection rt_fec(order, dim);
    mfem::ParFiniteElementSpace *rt_fes =
        new mfem::ParFiniteElementSpace(pmesh, &rt_fec);
    mfem::L2_FECollection l2_fec(order, dim);
    mfem::ParFiniteElementSpace *l2_fes =
        new mfem::ParFiniteElementSpace(pmesh, &l2_fec);
    mfem::FiniteElementCollection *fec;
    mfem::ParFiniteElementSpace *fes;
    switch (formulation)
    {
        case 0:
        {
            fec = new mfem::ND_FECollection(order, dim);
            fes = new mfem::ParFiniteElementSpace(pmesh, fec);
            break;
        }
        case 1:
        {
            fec = new mfem::H1_FECollection(order, dim);
            fes = new mfem::ParFiniteElementSpace(pmesh, fec, dim);
            break;
        }
        default:
        {
            delete pmesh;
            MPI_Finalize();
            return 4;
        }
    }

    HYPRE_Int global_vSize = fes->GlobalTrueVSize();
    if (myid == 0)
    {
        std::cout << "Number of unknowns: " << global_vSize << std::endl;
    }
    for (int i = 0; i < num_procs; i++)
    {
        MPI_Barrier(pmesh->GetComm());
        if (i == myid)
            std::cout << "Number of unkonwns on process " << myid << ": "
                      << fes->TrueVSize() << std::endl;
    }

    /* 8. Set up and assemble the parallel bilinear forms (and the parallel
     *    hypre matrices) corresponding to the Galerkin H(curl) formulation. */
    mfem::VectorFunctionCoefficient b0(dim, b0field_function);
    mfem::VectorFunctionCoefficient b_init(dim, binit_function);
    mfem::ConstantCoefficient kdivb(-kdivb_in);
    mfem::ConstantCoefficient zero(0.0);

    mfem::ParBilinearForm *m = new mfem::ParBilinearForm(fes);
    switch (formulation)
    {
        case 0:
        {
            m->AddDomainIntegrator(new mfem::MixedVectorMassIntegrator);
            break;
        }
        case 1:
        {
            m->AddDomainIntegrator(new mfem::VectorMassIntegrator);
            break;
        }
        default:
        {
            delete m;
            delete fes;
            delete fec;
            delete pmesh;
            MPI_Finalize();
            return 5;
        }
    }
    mfem::ParBilinearForm *k = new mfem::ParBilinearForm(fes);
    if (formulation == 1)
    {
    }
    switch (formulation)
    {
        case 0:
        {
            k->AddDomainIntegrator(new mfem::MixedCrossCurlCurlIntegrator(b0));
            break;
        }
        case 1:
        {
            k->AddDomainIntegrator(new mfem::VectorCrossCurlCurlIntegrator(b0));
            k->AddDomainIntegrator(new mfem::ElasticityIntegrator(kdivb, zero));
            break;
        }
        default:
        {
            delete k;
            delete m;
            delete fes;
            delete fec;
            delete pmesh;
            MPI_Finalize();
            return 6;
        }
    }
    mfem::ParMixedBilinearForm *div =
        new mfem::ParMixedBilinearForm(rt_fes, l2_fes);
    div->AddDomainIntegrator(new mfem::DivergenceInterpolator());

    // std::cout << "Before m->Assemble()." << std::endl;
    m->Assemble();
    m->Finalize();
    // std::cout << "After m->Assemble(), before k->Assemble()." << std::endl;
    k->Assemble(0);
    k->Finalize(0);
    // std::cout << "After k->Assemble()." << std::endl;
    div->Assemble();
    div->Finalize();

    mfem::HypreParMatrix *M = m->ParallelAssemble();
    mfem::HypreParMatrix *K = k->ParallelAssemble();
    mfem::HypreParMatrix *DIV = div->ParallelAssemble();

    /* 9. Define the initial conditions and initialize GLVis visualization. */
    mfem::ParGridFunction *b = new mfem::ParGridFunction(fes);
    mfem::VectorGridFunctionCoefficient b_coeff(b);
    mfem::ParGridFunction *b_rt = new mfem::ParGridFunction(rt_fes);
    mfem::ParGridFunction *divb = new mfem::ParGridFunction(l2_fes);
    b->ProjectCoefficient(b_init);
    b_rt->ProjectCoefficient(b_coeff);
    mfem::HypreParVector *B = b->GetTrueDofs();
    mfem::HypreParVector *B_RT = b_rt->GetTrueDofs();
    mfem::HypreParVector *DIVB = divb->GetTrueDofs();
    DIV->Mult(*B_RT, *DIVB);
    divb->SetFromTrueDofs(*DIVB);
    double div_error = ParNormlp(*DIVB, 2.0, DIV->GetComm());
    if (myid == 0) std::cout << "Divergence error: " << div_error << std::endl;

    {
        std::ostringstream mesh_name, sol_name;
        mesh_name << "helicon-mesh." << std::setfill('0') << std::setw(6) << myid;
        sol_name << "helicon-init." << std::setfill('0') << std::setw(6) << myid;
        std::ofstream omesh(mesh_name.str().c_str());
        omesh.precision(precision);
        pmesh->Print(omesh);
        std::ofstream osol(sol_name.str().c_str());
        osol.precision(precision);
        b->Save(osol);
    }

    mfem::socketstream sout;
    mfem::socketstream dbout;
    if (visualization)
    {
        char vishost[] = "localhost";
        int  visport   = 19916;
        sout.open(vishost, visport);
        if (!sout)
        {
            if (myid == 0)
            std::cout << "Unable to connect to GLVis server at "
                      << vishost << ':' << visport << std::endl;
            visualization = false;
            if (myid == 0)
            {
                std::cout << "GLVis visualization disabled.\n";
            }
        }
        else
        {
            sout << "parallel " << num_procs << " " << myid << "\n";
            sout.precision(precision);
            sout << "solution\n" << *pmesh << *b
                << "window_title 'Magnetic Field'" << std::endl;
            sout << "pause\n";
            sout << std::flush;
        }
        MPI_Barrier(pmesh->GetComm());
        dbout.open(vishost, visport);
        if (!dbout)
        {
            if (myid == 0)
            std::cout << "Unable to connect to GLVis server at "
                      << vishost << ':' << visport << std::endl;
            visualization = false;
            if (myid == 0)
            {
                std::cout << "GLVis visualization disabled.\n";
            }
        }
        else
        {
            if (myid == 0)
            std::cout << "GLVis visualization paused."
                      << " Press space (in the GLVis window) to resume it.\n";
            dbout << "parallel " << num_procs << " " << myid << "\n";
            dbout.precision(precision);
            dbout << "solution\n" << *pmesh << *divb
                << "window_title 'Divergence Error'" << std::endl;;
            dbout << "pause\n";
            dbout << std::flush;
            if (myid == 0)
            std::cout << "GLVis visualization paused."
                      << " Press space (in the GLVis window) to resume it.\n";
        }
    }

    /* 10. Define the time-dependent evolution operator describing the ODE and
     *     perform time integration. */
    Galerkin galerkin_adv(*M, *K);

    double t = 0.0;
    galerkin_adv.SetTime(t);
    ode_solver->Init(galerkin_adv);

    bool done = false;
    for (int ti = 0; !done; )
    {
        double dt_real = std::min(dt, t_final - t);
        ode_solver->Step(*B, t, dt_real);
        ti++;

        // 11. Extract the parallel grid function corresponding to the finite
        //     element approximation U (the local solution on each processor).
        b->SetFromTrueDofs(*B);
        b_rt->ProjectCoefficient(b_coeff);
        b_rt->GetTrueDofs(*B_RT);
        DIV->Mult(*B_RT, *DIVB);
        divb->SetFromTrueDofs(*DIVB);
        div_error = ParNormlp(*DIVB, 2.0, DIV->GetComm());
        if (myid == 0) std::cout << "Divergence error: " << div_error << std::endl;

        done = (t >= t_final - 1e-8*dt);

        if (done || ti % vis_steps == 0)
        {
            if (myid == 0)
            {
                std::cout << "time step: " << ti << ", time: " << t << std::endl;
            }

            if (visualization)
            {
                sout << "parallel " << num_procs << " " << myid << "\n";
                sout << "solution\n" << *pmesh << *b
                    << "window_title 'Magnetic Field'" << std::flush;
                MPI_Barrier(pmesh->GetComm());
                dbout << "parallel " << num_procs << " " << myid << "\n";
                dbout << "solution\n" << *pmesh << *divb
                    << "window_title 'Divergence Error'" << std::flush;
            }
        }
    }

    // ?. Free the used memory.
    delete B;
    delete b;
    delete k;
    delete m;
    delete fes;
    delete fec;
    delete pmesh;
    delete ode_solver;

    MPI_Finalize();
    return 0;
}

Galerkin::Galerkin(mfem::HypreParMatrix &_M, mfem::HypreParMatrix &_K)
    : TimeDependentOperator(_M.Width()), M(_M), K(_K), T(nullptr),
      // T_prec(),
      T_solver(M.GetComm()), z(_M.Width())
{
    T_prec.SetType(mfem::HypreSmoother::Jacobi);
    T_solver.iterative_mode = false;
    T_solver.SetAbsTol(1.e-8);
    // T_solver.SetRelTol(1.e-10);
    T_solver.SetMaxIter(2000);
    T_solver.SetPrintLevel(3);
    T_solver.SetPreconditioner(T_prec);
}

void Galerkin::ImplicitSolve(const double dt, const mfem::Vector &b,
    mfem::Vector &db_dt)
{
    if(!T || dt != current_dt)
    {
        if(T) delete T;
        T = Add(1.0, M, -dt, K);
        // T->Print("T_mat");
        current_dt = dt;
        T_solver.SetOperator(*T);
    }
    // T->Mult(b, z);
    // for(int i = 0; i < num_procs; i++) {
    //     MPI_Barrier(MPI_COMM_WORLD);
    //     if (i == myid) {
    //         z.Print_HYPRE(std::cout);
    //     }
    // }
    K.Mult(b, z);
    // for(int i = 0; i < num_procs; i++) {
    //     MPI_Barrier(MPI_COMM_WORLD);
    //     if (i == myid) {
    //         z.Print_HYPRE(std::cout);
    //     }
    // }
    T_solver.Mult(z, db_dt);
    // for(int i = 0; i < num_procs; i++) {
    //     MPI_Barrier(MPI_COMM_WORLD);
    //     if (i == myid) {
    //         db_dt.Print_HYPRE(std::cout);
    //     }
    // }
}

Galerkin::~Galerkin()
{
    delete T;
}

void b0field_function(const mfem::Vector &x, mfem::Vector &b)
{
   int dim = x.Size();

   // map to the reference [-1,1] domain
   mfem::Vector X(dim);
   for (int i = 0; i < dim; i++)
   {
      double center = (bb_min[i] + bb_max[i]) * 0.5;
      X(i) = 2 * (x(i) - center) / (bb_max[i] - bb_min[i]);
   }
   switch (problem)
   {
      default:
      {
         switch (dim)
         {
            case 3: b(2) = 0.0;
            case 2: b(1) = 2.0/pi*std::cos(pi*alpha);
            case 1: b(0) = -2.0/pi*std::sin(pi*alpha);
            break;
         }
         break;
      }
   }
}

void binit_function(const mfem::Vector &x, mfem::Vector &b)
{
   int dim = x.Size();

   // map to the reference [-1,1] domain
   mfem::Vector X(dim);
   for (int i = 0; i < dim; i++)
   {
      double center = (bb_min[i] + bb_max[i]) * 0.5;
      X(i) = 2 * (x(i) - center) / (bb_max[i] - bb_min[i]);
   }
   switch (problem)
   {
      default:
      {
         switch (dim)
         {
            case 3: b(2) =       std::cos(pi*(ny*X(0)+nz*X(1)));
            case 2: b(1) =  ny/n*std::sin(pi*(ny*X(0)+nz*X(1)));
            case 1: b(0) = -nz/n*std::sin(pi*(ny*X(0)+nz*X(1)));
            break;
         }
         break;
      }
   }
}
